import fr.ufc.l3info.oprog.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.*;

public class TestsFonctionnels_fp {

    private Exploitant exp;
    private Ville ville;
    private FabriqueVelo fabrique;

    final static String VALID_RIB = "12345-98765-12345678912-21";

    @Before
    public void init() throws IOException {
        this.exp = new Exploitant();
        this.ville = new Ville();
        this.ville.initialiser(new File("./target/classes/data/stationsBesancon.txt"));
        this.ville.setStationPrincipale(this.ville.getStation("Bersot"));
        this.fabrique = FabriqueVelo.getInstance();
    }

    @Test
    public void creationRepartitionVelos_pasAssez(){

        for(int i = 0 ; i < 100 ; ++i){
            IVelo velo = this.fabrique.construire('j', Math.random() > .5 ? null : "SUSPENSION_AVANT" );
            assertNotNull(velo);
            this.exp.acquerirVelo(velo);
        }

        this.exp.ravitailler(ville);

        int nbVelos = 0;
        for(Station s : ville){
            //System.out.println(s.getNom()+" - c:"+s.capacite()+ "- bl:"+s.nbBornesLibres());
            assertTrue(s.nbBornesLibres() >= Math.floor(s.capacite() / 2.0));
            nbVelos += s.capacite() - s.nbBornesLibres();
        }

        assertEquals(100,nbVelos);

        assertEquals(3,this.ville.getStation("Mairie").nbBornesLibres());
        assertEquals(5,this.ville.getStation("8 Septembre").nbBornesLibres());
        assertEquals(9,this.ville.getStation("Morand").nbBornesLibres());
    }

    public void remplirMax(){
        for(int i = 0 ; i < 144 ; ++i){
            IVelo velo = this.fabrique.construire('j', Math.random() > .5 ? null : "SUSPENSION_AVANT" );
            assertNotNull(velo);
            this.exp.acquerirVelo(velo);
        }
        this.exp.ravitailler(ville);
    }

    @Test
    public void creationRepartitionVelos_trop(){

        remplirMax();
        int nbVelos = 0;
        for(Station s : ville){
            //System.out.println(s.getNom()+" - c:"+s.capacite()+ "- bl:"+s.nbBornesLibres());
            assertEquals(s.nbBornesLibres(), Math.floor(s.capacite() / 2.0), 0.0);
            nbVelos += s.capacite() - s.nbBornesLibres();
        }
        assertEquals(144,nbVelos);
    }

    @Test
    public void collecterVelosRienAChanger(){
        remplirMax();

        int nbVelos = 0;
        for(Station s : ville){
            //System.out.println(s.getNom()+" - c:"+s.capacite()+ "- bl:"+s.nbBornesLibres());
            assertEquals(s.nbBornesLibres(), Math.floor(s.capacite() / 2.0), 0.0);
            nbVelos += s.capacite() - s.nbBornesLibres();
        }
        assertEquals(144,nbVelos);
    }

    @Test
    public void ravitailler(){
        remplirMax();

        Station bersot = this.ville.getStation("Bersot");
        int i = 1;
        while(i < bersot.capacite() && bersot.veloALaBorne(i) == null){ i++;}
        assertNotEquals(i,bersot.capacite());
        bersot.veloALaBorne(i).abimer();
        i++;
        while(i < bersot.capacite() && bersot.veloALaBorne(i) == null){ i++;}
        assertNotEquals(i,bersot.capacite());
        IVelo velo = bersot.emprunterVelo(ville.creerAbonne("john",VALID_RIB),i);
        velo.parcourir(1000);
        bersot.arrimerVelo(velo,i);


        Station viotte = this.ville.getStation("Gare Viotte");
        i = 1;
        while(i < viotte.capacite() && viotte.veloALaBorne(i) == null){ i++;}
        assertNotEquals(i,viotte.capacite());
        viotte.veloALaBorne(i).abimer();


        this.exp.acquerirVelo(this.fabrique.construire('f'));
        this.exp.acquerirVelo(this.fabrique.construire('f'));
        this.exp.ravitailler(ville);

        int nbVelos = 0;
        for(Station s : ville){
            //System.out.println(s.getNom()+" - c:"+s.capacite()+ "- bl:"+s.nbBornesLibres());
            assertTrue(s.nbBornesLibres() >= Math.floor(s.capacite() / 2.0));
            nbVelos += s.capacite() - s.nbBornesLibres();

            for(int b = 1 ; b <= s.capacite() ; ++b){
                IVelo v = s.veloALaBorne(b);
                if(v == null){
                    //System.out.println("-");
                    continue;
                }
                //System.out.println(v+(v.estAbime() ? " - a" : ""));
                assertFalse(v.estAbime());
                assertTrue(v.prochaineRevision() >= 0);
            }
        }
        assertEquals(143,nbVelos);

        this.exp.entretenirVelos();
        this.exp.ravitailler(ville);

        nbVelos = 0;
        for(Station s : ville){
            //System.out.println(s.getNom()+" - c:"+s.capacite()+ "- bl:"+s.nbBornesLibres());
            assertEquals(s.nbBornesLibres(), Math.floor(s.capacite() / 2.0), 0.0);
            nbVelos += s.capacite() - s.nbBornesLibres();

            for(int b = 1 ; b <= s.capacite() ; ++b){
                IVelo v = s.veloALaBorne(b);
                if(v == null){
                    //System.out.println("-");
                    continue;
                }
                //System.out.println(v+(v.estAbime() ? " - a" : ""));
                assertFalse(v.estAbime());
                assertTrue(v.prochaineRevision() >= 0);
            }
        }
        assertEquals(144,nbVelos);
    }

    @Test
    public void ravitailler_tousAbime(){
        remplirMax();

        for(Station s : this.ville){
            for(int i = 1 ; i <= s.capacite() ; ++i){
                IVelo velo = s.veloALaBorne(i);
                if(velo != null){
                    velo.abimer();
                }
            }
        }

        this.exp.ravitailler(ville);

        for(Station s : this.ville){
            assertEquals(s.capacite(),s.nbBornesLibres());
        }

    }

    @Test
    public void ravitailler_tousAReviser(){
        remplirMax();

        for(Station s : this.ville){
            for(int i = 1 ; i <= s.capacite() ; ++i){
                IVelo velo = s.veloALaBorne(i);
                if(velo != null){
                    velo = s.emprunterVelo(ville.creerAbonne("John",VALID_RIB),i);
                    velo.parcourir(1000);
                    s.arrimerVelo(velo,i);
                }
            }
        }

        this.exp.ravitailler(ville);

        for(Station s : this.ville){
            assertEquals(s.nbBornesLibres(), Math.floor(s.capacite() / 2.0), 0.0);
        }

    }


    @Test
    public void ravitailler2() throws IOException {

        this.ville.initialiser(new File("./target/classes/data/stationsBesancon3.txt"));
        this.ville.setStationPrincipale(this.ville.getStation("Granvelle"));

        for(int i = 0 ; i < 15 ; ++i){
            IVelo velo = fabrique.construire('h');
            if( i > 13){
                velo.estAbime();
            }else if(i > 7){
                velo.parcourir(1000);
            }
            this.exp.acquerirVelo(velo);
        }

        this.exp.ravitailler(ville);

        Station granvelle = this.ville.getStation("Granvelle");
        assertEquals(granvelle.nbBornesLibres(), 4);

        Station stJacques = this.ville.getStation("Saint Jacques");
        assertEquals(stJacques.nbBornesLibres(), 6);

        Station viotte = this.ville.getStation("Gare Viotte");
        assertEquals(viotte.nbBornesLibres(), 10);

        this.exp.entretenirVelos();
        this.exp.ravitailler(ville);

        granvelle = this.ville.getStation("Granvelle");
        assertEquals(granvelle.nbBornesLibres(), 4);

        stJacques = this.ville.getStation("Saint Jacques");
        assertEquals(stJacques.nbBornesLibres(), 5);

        viotte = this.ville.getStation("Gare Viotte");
        assertEquals(viotte.nbBornesLibres(), 5);

    }



}

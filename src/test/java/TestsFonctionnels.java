import fr.ufc.l3info.oprog.*;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class TestsFonctionnels {
    private Ville besancon;
    private Abonne francois;

    private final String path = "./target/classes/data/";
    private final int NB_STATIONS_BESANCON = 30;
    private final String RIB_OK = "12345-98765-12345678912-21";

    @Before
    public void setUp() throws IOException {
        File f = new File(path + "stationsBesancon.txt");
        this.besancon = new Ville();
        this.besancon.initialiser(f);

        this.francois = this.besancon.creerAbonne("François", this.RIB_OK);
    }

    public int getBorneAvecVelo(Station st) {
        int index = 1;
        while (st.veloALaBorne(index) == null && index < st.capacite()) {
            ++index;
        }

        return index;
    }

    public int getBorneVide(Station st) {
        int index = 1;
        while (st.veloALaBorne(index) != null && index < st.capacite()) {
            ++index;
        }

        return index;
    }

    @Test
    public void testCreerVilleBesancon() {
        // -- Gestion des stations --

        int nbOfStations = 0;
        for (Station st : besancon) {
            Assert.assertNotNull(st);
            Assert.assertTrue(st.capacite() > 0);
            ++nbOfStations;
        }

        Assert.assertEquals(this.NB_STATIONS_BESANCON, nbOfStations);

        Station marulaz = this.besancon.getStation("Marulaz");
        this.besancon.setStationPrincipale(marulaz);
        Iterator<Station> iter = this.besancon.iterator();
        Assert.assertEquals(marulaz, iter.next());

        // -- Gestion des abonnées --

        this.besancon.creerAbonne("Fabian", this.RIB_OK);
        this.besancon.creerAbonne("Nathanaël", "FAITES-DES-SAUVEGARDES");
        Map<Abonne, Double> tousLesAbonnes = this.besancon.facturation(11, 2020);

        Assert.assertEquals(3, tousLesAbonnes.size());
        Set<Abonne> listeAbonnes = tousLesAbonnes.keySet();
        int nbBloques = 0;
        for (Abonne a : listeAbonnes) {
            if (a.estBloque()) {
                nbBloques++;
            }
        }
        Assert.assertEquals(1, nbBloques);
    }

    @Test
    public void testPromenadeEnVelo() {
        Exploitant veloCompany = new Exploitant();
        for (int i = 0; i < 1000; i++) {
            veloCompany.acquerirVelo(new Velo());
        }
        veloCompany.ravitailler(this.besancon);

        Iterator<Station> iter = this.besancon.iterator();
        Station st1 = iter.next();

        int index = getBorneAvecVelo(st1);
        IVelo v = st1.emprunterVelo(this.francois, index);
        Assert.assertNotNull(v);
        v.parcourir(Double.MAX_VALUE);
        Assert.assertTrue(v.prochaineRevision() < 0);
        Assert.assertEquals(0, st1.arrimerVelo(v, index));
    }

    @Test
    public void testPromenadeEntreStations() {
        Exploitant veloCompany = new Exploitant();
        for (int i = 0; i < 1000; i++) {
            veloCompany.acquerirVelo(new Velo());
        }
        veloCompany.ravitailler(this.besancon);

        Iterator<Station> iter = this.besancon.iterator();
        Station st1 = iter.next(), st2 = iter.next();

        IVelo v = st1.emprunterVelo(this.francois, getBorneAvecVelo(st1));
        Assert.assertNotNull(v);
        v.parcourir(Double.MAX_VALUE);

        Assert.assertTrue(v.prochaineRevision() < 0);
        Assert.assertEquals(0, st2.arrimerVelo(v, getBorneVide(st2)));
    }

    @Test
    public void testEmprunterUnVeloSurLaBonneBorne() {
        Exploitant veloCompany = new Exploitant();
        for (int i = 0; i < 1000; i++) {
            veloCompany.acquerirVelo(new Velo());
        }
        veloCompany.ravitailler(this.besancon);

        Iterator<Station> iter = this.besancon.iterator();
        Station st1 = iter.next(), st2 = iter.next();

        int index = 1, vindex = -1;
        while (index < st1.capacite()) {
            if (st1.veloALaBorne(index) != null) {
                if (vindex == -1) {
                    vindex = index;
                }
                ++index;
                continue;
            }

            Assert.assertNotEquals(0, st1.emprunterVelo(this.francois, index));
            ++index;
        }
        IVelo v = st1.emprunterVelo(this.francois, vindex);
        Assert.assertNotNull(v);
        v.parcourir(Double.MAX_VALUE);

        index = 1;
        vindex = -1;
        while (index < st2.capacite()) {
            if (st1.veloALaBorne(index) == null) {
                if (vindex == -1) {
                    vindex = index;
                }
                ++index;
                continue;
            }

            Assert.assertNotEquals(0, st1.arrimerVelo(v, index));
            ++index;
        }
        Assert.assertTrue(v.prochaineRevision() < 0);
        Assert.assertEquals(0, st2.arrimerVelo(v, index));
    }

    @Test
    public void testPromenadeVeloAbime() {
        Exploitant veloCompany = new Exploitant();
        for (int i = 0; i < 1000; i++) {
            veloCompany.acquerirVelo(new Velo());
        }
        veloCompany.ravitailler(this.besancon);

        Iterator<Station> iter = this.besancon.iterator();
        Station st1 = iter.next();

        int index = getBorneAvecVelo(st1);
        IVelo v = st1.emprunterVelo(this.francois, index);
        Assert.assertNotNull(v);
        v.parcourir(Double.MAX_VALUE);
        Assert.assertTrue(v.prochaineRevision() < 0);
        v.abimer();
        Assert.assertTrue(v.estAbime());
        Assert.assertEquals(0, st1.arrimerVelo(v, index));
    }

    @Test
    public void testPromenadeVersLePlusProche() {
        Exploitant veloCompany = new Exploitant();
        for (int i = 0; i < 1000; i++) {
            veloCompany.acquerirVelo(new Velo());
        }
        veloCompany.ravitailler(this.besancon);

        Iterator<Station> iter = this.besancon.iterator();

        IVelo v = null;

        Station oldStation = null;
        while (iter.hasNext()) {
            Station s = iter.next();

            if (v != null) {
                Assert.assertEquals(0, s.arrimerVelo(v, getBorneVide(s)));
            }
            v = s.emprunterVelo(this.francois, getBorneAvecVelo(s));
            Assert.assertNotNull(v);

            if (oldStation != null) {

                Station meilleureStation = null;
                double meilleureDistance = 0;
                Iterator<Station> rechercheIter = this.besancon.iterator();
                while (rechercheIter.hasNext()) {
                    Station actuelle = rechercheIter.next();
                    if (actuelle == oldStation) continue;
                    double distance = oldStation.distance(actuelle);
                    if (meilleureStation == null || distance < meilleureDistance) {
                        meilleureDistance = distance;
                        meilleureStation = s;
                    }
                }

                Assert.assertEquals(s, meilleureStation);

            }

            oldStation = s;
        }
    }

    @Test
    public void testCycleDeVieStation() {
        Station marulaz = this.besancon.getStation("Marulaz");
        this.besancon.setStationPrincipale(marulaz);

        this.besancon.creerAbonne("Fabian", this.RIB_OK);
        this.besancon.creerAbonne("Nathanaël", "FAITES-DES-SAUVEGARDES");

        Abonne nath = this.besancon.creerAbonne("Nath", this.RIB_OK);
        Assert.assertFalse(nath.estBloque());

        Exploitant veloCompany = new Exploitant();
        for (int i = 0; i < 1000; i++) {
            veloCompany.acquerirVelo(new Velo());
        }
        veloCompany.ravitailler(this.besancon);
        for (Station st: this.besancon) {
            Assert.assertNotEquals(st.capacite(), st.nbBornesLibres());
        }

        Iterator<Station> iter = this.besancon.iterator();

        Station st1 = iter.next();

        int indexNath = getBorneAvecVelo(st1);
        IVelo vNath = st1.emprunterVelo(nath, indexNath);
        vNath.abimer();
        Assert.assertEquals(0, st1.arrimerVelo(vNath, indexNath));
        Assert.assertTrue(st1.veloALaBorne(indexNath).estAbime());
        veloCompany.ravitailler(this.besancon);
        Assert.assertFalse(st1.veloALaBorne(indexNath).estAbime());

        IVelo vFrançois = st1.emprunterVelo(francois, indexNath);
        Assert.assertNotNull(vFrançois);
        Station st2 = iter.next();
        int indexFran = getBorneVide(st2);
        Assert.assertEquals(0, st2.arrimerVelo(vFrançois, indexFran));
    }
}
